/*
 * title
 * copyright: Kogia_sima 2018
 */

#include <cstring>
#include <stdexcept>
#include "StringFilter/SQLFilter.hpp"

SQLFilter::SQLFilter(Node* node, const char* column_name) {
  M_root = node;
  M_column_name = column_name;
  M_column_length = strlen(column_name);
}

std::string SQLFilter::str() const {
  int length = calc_sql_length(M_root);
  std::string result;
  result.resize(length);
  char* ptr = &(result[0]);
  str_recursive(M_root, ptr);

  return result;
}

int SQLFilter::calc_sql_length(Node* const node) const {
  switch (node->type()) {
    case NodeType::POSITIVE:
      return node->getValue().size() + M_column_length + 10;

    case NodeType::NEGATIVE:
      return node->getValue().size() + M_column_length + 14;

    case NodeType::AND:
      return (calc_sql_length(node->getLeft()) +
              calc_sql_length(node->getRight()) + 9);

    case NodeType::OR:
      return (calc_sql_length(node->getLeft()) +
              calc_sql_length(node->getRight()) + 8);

    default:
      throw std::runtime_error(
          "Invalid node detected while generating sql filter");
  }
}

char* SQLFilter::str_recursive(Node* const node, char* ptr) const {
  switch (node->type()) {
    case NodeType::NEGATIVE:
      strncpy(ptr, "NOT ", 4);
      ptr += 4;

    case NodeType::POSITIVE:
      strncpy(ptr, M_column_name, M_column_length);
      ptr += M_column_length;

      strncpy(ptr, " LIKE \"%", 8);
      ptr += 8;

      strncpy(ptr, node->getValue().data(), node->getValue().size());
      ptr += node->getValue().size();

      strncpy(ptr, "%\"", 2);
      ptr += 2;

      return ptr;

    case NodeType::AND:
      *(ptr++) = '(';

      ptr = str_recursive(node->getLeft(), ptr);

      strncpy(ptr, ") AND (", 7);
      ptr += 7;

      ptr = str_recursive(node->getRight(), ptr);
      *(ptr++) = ')';

      return ptr;

    case NodeType::OR:
      *(ptr++) = '(';

      ptr = str_recursive(node->getLeft(), ptr);

      strncpy(ptr, ") OR (", 6);
      ptr += 6;

      ptr = str_recursive(node->getRight(), ptr);
      *(ptr++) = ')';

      return ptr;

    default:
      throw std::runtime_error(
          "Invalid node detected while generating sql filter");
  }
}
