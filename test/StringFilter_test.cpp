/*
 * StringFilter test program
 * copyright: Kogia-sima 2018
 */

#include <gtest/gtest.h>
#include <StringFilter/StringFilter.hpp>

TEST(valid_test, applicable) {
  StringFilter sf1("rabbit and turtle");
  EXPECT_TRUE(sf1.is_valid("rabbit and turtle"));
  EXPECT_FALSE(sf1.is_valid("Sea turtles were released to the ocean."));

  StringFilter sf2("not 親子 or 親子丼");
  EXPECT_FALSE(sf2.is_valid("親子におすすめ"));
  EXPECT_TRUE(sf2.is_valid("親子丼 大盛り"));
}

TEST(valid_test, null_str) {
  StringFilter sf1("");
  EXPECT_TRUE(sf1.is_valid(
      "Buffalo buffalo Buffalo buffalo buffalo buffalo Buffalo buffalo."));

  StringFilter sf2("buffalo");
  EXPECT_FALSE(sf2.is_valid(""));
}

TEST(sql_test, test1) {
  StringFilter sf("rabbit and turtle");
  std::string sql_str = sf.sql_string("col");
  EXPECT_EQ(sql_str, "(col LIKE \"%rabbit%\") AND (col LIKE \"%turtle%\")");
}

TEST(sql_test, test2) {
  StringFilter sf("APPLE or (not ORANGE and STRAWBERRY)");
  std::string sql_str = sf.sql_string("col");
  EXPECT_EQ(sql_str,
            "(col LIKE \"%APPLE%\") OR ((NOT col LIKE \"%ORANGE%\") AND (col "
            "LIKE \"%STRAWBERRY%\"))");
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
