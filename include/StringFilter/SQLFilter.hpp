/*
 * SQLFilter class declaration
 * copyright: Kogia_sima 2018
 */

#ifndef STRINGFILTER_SQLFILTER_HPP
#define STRINGFILTER_SQLFILTER_HPP

#include <string>
#include "Node.hpp"

class SQLFilter {
 public:
  SQLFilter() = default;
  explicit SQLFilter(Node* node, const char* column_name);
  std::string str() const;

 protected:
  int calc_sql_length(Node* const node) const;
  char* str_recursive(Node* const node, char* ptr) const;

 private:
  Node* M_root;
  const char* M_column_name;
  int M_column_length;
};

#endif
